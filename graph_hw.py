# graficar el recorrido de alguno autos para ver como se mueven.

from matplotlib import pyplot as plt
path = 'D:/Memoria/Avances Semanales/Documentos/register_site_17.csv'
file = open(path)
vehicle_register = {} # 'id_vehicle' = [[], []] x,y
file.readline()
for line in file:
	#print(line.split())
	content = line.split(';')
	#print(len(content))
	if content[0] not in vehicle_register:
		vehicle_register[content[0]] = [[], []]
	vehicle_register[content[0]][0].append(int(content[5]))
	vehicle_register[content[0]][1].append(int(content[4]))
# faltaria dibujar las variables de las pistas
#print(vehicle_register)
plt.plot([ 298, 126], [201, 479], 'b')
plt.plot([ 321, 264], [200, 479], 'b')
plt.plot([ 344, 437], [198, 477], 'b')
plt.plot([ 367, 583], [199, 477], 'b')	
legend = ['line_1', 'line_2', 'line_3', 'line_4']	
for id_vehicle in vehicle_register:
	if id_vehicle in ['17.0', '100.0', '181.0', '247.0', '290.0']:
		plt.plot((vehicle_register[id_vehicle][0]), (vehicle_register[id_vehicle][1]))
		legend.append(id_vehicle)
plt.legend(legend)
plt.grid(True)
plt.axis([0, 640, 0, 480])
plt.show()	
