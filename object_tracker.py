from models import *
from utils import *

import os, sys, time, datetime, random
import torch
import numpy as np
import cv2
import os.path as ops
import json
from sort import *
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
from torch.autograd import Variable

from PIL import Image

from lane_detection.main import LaneDetector



# load weights and set defaults
config_path='config/yolov3.cfg'
weights_path='config/yolov3.weights'
class_path='config/coco.names'
img_size=416
conf_thres=0.8
nms_thres=0.03

# load model and put into eval mode
model = Darknet(config_path, img_size=img_size)
model.load_weights(weights_path)
model.cuda()
model.eval()

classes = utils.load_classes(class_path)
Tensor = torch.cuda.FloatTensor

# variables globales que pueden servir en el fururo.

# tuvimos que cambiar esta variable para mejorar el metodo de detección.
# la nueva forma sera {id: [[times], [points]]} -> points[[point_before], [point_after]]

# si se quiere detectar en mas puntos se puede hacer agrandando este diccionario.

state_cross = {'First': False, 'Second': False}


def detect_image(img):
    '''
     scale and pad image

    '''
    ratio = min(img_size/img.size[0], img_size/img.size[1])
    imw = round(img.size[0] * ratio)
    imh = round(img.size[1] * ratio)
    img_transforms = transforms.Compose([ transforms.Resize((imh, imw)),
         transforms.Pad((max(int((imh-imw)/2),0), max(int((imw-imh)/2),0), max(int((imh-imw)/2),0), max(int((imw-imh)/2),0)),
                        (128,128,128)),
         transforms.ToTensor(),
         ])
    # convert image to Tensor
    image_tensor = img_transforms(img).float()
    image_tensor = image_tensor.unsqueeze_(0)
    input_img = Variable(image_tensor.type(Tensor))
    # run inference on the model and get detections
    with torch.no_grad():
        detections = model(input_img)
        detections = utils.non_max_suppression(detections, 80, conf_thres, nms_thres)
    return detections[0]


def interpolation(points, time, measuring_point): 
    '''
     point: [[x0, y0, x1, y1], [x0, y0, x1, y1]], time = [first_time, second_time], measuring_point = exact value where the y-axis collides(int in pixel)
     generates an approximation at the precise instant that the line crosses
     3d incorporate time
    '''
    x0, y0, x1, y1,  = points[0] # first detection
    x0_1, y0_1, x2, y2 = points[1] # second detection
    time1 , time2 = time
    if (y0 == y0_1) or (y1 == y2) or (time1 == time2):
        tI = time[0]
        xS, yS, xI, yI = points[0]
    else:
        xI = int(((x2 - x1) * (measuring_point - y1) / (y2 - y1)) + x1)
        yI = measuring_point
        tI = ((time2 - time1) * (measuring_point - y1) / (y2 -y1)) + time1
        yS = int(((tI - time1) * ( y0_1 - y0) / (time2 - time1)) + y0)
        xS = int(((x0_1 - x0) * (yS - y0) / (y0_1 - y0)) + x0)
    
    return [xS , yS, xI, yI, tI] 



def detect_line_crossing( points, frames, id_car, document, date_detect, car_registration, car_registration_write, state): #array con los autos detectados, frames en que va, ancho de la pista en metros.
    # cruce por la linea central, función provisoria para verificar el primer hito.
    global state_cross
    x_0, y_0, x_1, y_1 = points
    # linea izquierda, linea derecha y linea horizontal.
    left_line, rigth_line, line_y, track_width_meters = date_detect # deberia ser como las cordenadas del punto de intervención.
    
    track_center = ((rigth_line - left_line) / 2) + left_line
    crossover_time = (frames/30) # es el tiempo en cantidad de segundos donde se corta la pista.
    detection_difference = 15 # cantidad de frames donde se estima la cercania de paso del automovil 
    track_width = rigth_line - left_line # ancho de pista en pixeles

    # ahora que tengo las dimensiones solo me interesa ajustar el eje x, por lo tanto debemos calcular
    # y definir donde esta la pista del medio.
    # El siguiente define si esta en intervalo que se definio para la pista.

    if line_y <= y_1:  #and  (left_line <= lower_center <= rigth_line):
        # aca tiene que estar dentro de la pista.
        if left_line <= int((x_1 + x_0)/2) <= rigth_line: #and rigth_line >= x_1:
            # aca si vuelve a detectar que el auto esta antes de cruzar la linea reemplaza el valor anterior
            # ya que es el mas cercano a la linea
            if str(id_car) in car_registration:
                # actualizacion de tiempos
                car_registration[str(id_car)][0].pop(0) # borrando el tiempo anterior
                car_registration[str(id_car)][0].append(crossover_time)
                # actualizacion de puntos
                car_registration[str(id_car)][1].pop(0)
                car_registration[str(id_car)][1].append([x_0, y_0, x_1, y_1])
            # si mi auto nunca  a sido registrado lo mas conveniente 
            # es guardar su primera detección antes de cruzar 
            if str(id_car) not in car_registration:
                car_registration[str(id_car)] = [[], []]
                car_registration[str(id_car)][0].append(crossover_time)
                car_registration[str(id_car)][1].append([x_0, y_0, x_1, y_1])

    # aca esta la primera deteccion antes de que cruce la linea, es muy importante por que es la que
    # permite interpolar para calular ] tiempo
    if y_1 <= line_y:
        # aca tiene que estar dentro de la pista.
        if left_line <= int((x_1 + x_0)/2) <= rigth_line and str(id_car) in car_registration and len(car_registration[str(id_car)][0]) < 2 :

                car_registration[str(id_car)][0].append(crossover_time)
                car_registration[str(id_car)][1].append([x_0, y_0, x_1, y_1])
                print('la id del auto es: ', id_car, '\n')
                print('los puntos son: ', car_registration[str(id_car)][1])
                x_n_0, y_n_0, x_n_1, y_n_1, new_time_cross = interpolation(points = car_registration[str(id_car)][1],
                                                                        time = car_registration[str(id_car)][0],
                                                                        measuring_point = date_detect[2])
                w_d = int((x_n_1 - x_n_0) / 2)
                lower_center = int(x_n_0) + w_d
                vehicle_width = x_n_1 - x_n_0
                distance = -1 * (track_center - lower_center)
                distance_meters = (track_width_meters*distance/track_width)
                car_registration_write[str(id_car)] = [new_time_cross, distance_meters, (x_n_0, y_n_0, x_n_1, y_n_1)]
                car_registration[str(id_car)].append([new_time_cross, distance, distance_meters])
                if state == 'First':
                    register_cross.write('{0};{1};{2};{3};{4}\n'.format(id_car, new_time_cross, distance, distance_meters, vehicle_width))
                state_cross[state] = True


def save_image(frame, date_detect, state):
    '''
        guarda la imagen del cruce del auto por la linea
    '''
    cv2.line(frame, (date_detect[0], date_detect[2]), (date_detect[1], date_detect[2]), (255, 0, 0), 5) # linea horizontal
    cv2.line(frame, (date_detect[0], 0), (date_detect[0], vw), (255, 0, 0), 5)
    cv2.line(frame, (date_detect[1], 0), (date_detect[1], vh), (255, 0, 0), 5)
    cv2.polylines(frame, [np.array(frame_lanes[0])], False, (0, 0, 255), 5)
    cv2.polylines(frame, [np.array(frame_lanes[1])], False, (0, 0, 255), 5)
    cv2.polylines(frame, [np.array(frame_lanes[2])], False, (0, 0, 255), 5)
    cv2.polylines(frame, [np.array(frame_lanes[3])], False, (0, 0, 255), 5)
    
    cv2.imwrite(f'data_register/{video_status["site"]}/image_cross/{frames/30}.png', frame)
    state_cross[state] = False


def write_two_detection(car_registration_first, car_registration_second, path_name):
    '''
        escribe un ducumento donde se guardan ambos registros
    '''
    two_detection = open(f'data_register/{video_status["site"]}/detection_two_points_{path_name}.csv', 'w')
    two_detection.write('car_i;time_first;time_second;pixel_f_d;pixel_s_d;meters_f_d;meters_s_d\n')
    for car in car_registration_first:
        if car in car_registration_second and len(car_registration_second[car][0])>1 and len(car_registration_first[car][0])>1:

            car_one = car_registration_first[car][2]
            car_two = car_registration_second[car][2]
            two_detection.write('{0};{1};{2};{3};{4};{5};{6}\n'.format(car, car_one[0], car_two[0], car_one[1], car_two[1], car_one[2], car_two[2]))
    
    two_detection.close()

def find_x_coordinate(transpose_lane_points, y_coordinate):
    '''

    '''
    index = transpose_lane_points['y'].index(y_coordinate)
    return transpose_lane_points['x'][index]

def review_order(vertices_coordinates):
    '''
    check order vertices_coordinates
    '''
    x_point = sorted([x_point['max'][0] for x_point in vertices_coordinates.values()])
    register = [0]*(len(vertices_coordinates.keys()))
    for k, v in vertices_coordinates.items(): 
        if  v['max'][0] in x_point:
            register[x_point.index(v['max'][0])] = k
    return register


def visual_adjustment(vertices_coordinates):
    '''
    I calculate to adjust the polygon to the height where the track ends according to its lines.
    Returns two points x (left, right)

    '''
    order_keys = review_order(vertices_coordinates)
    left_line_x = vertices_coordinates[order_keys[0]]['max'][0]
    rigth_line_x = vertices_coordinates[order_keys[-1]]['max'][0]
    distance_x = int((rigth_line_x - left_line_x) / 2)
    return left_line_x + 0.95 * distance_x, rigth_line_x - 0.95 * distance_x

def interpolation_2d(vertices_coordinates, lines, vh):
    '''
    This interpolation serves to define well the point where the automobile crossing is going to be measured.
    '''
    half = vh / 2
    three_quarters = 3 * vh / 4
    set_lines = []
    for value in  [half, three_quarters]:
        print(f'value : {value}')
        middle_lines = review_order(vertices_coordinates)[1:3]
        x = []
        for line in middle_lines:
            y_point = min([y for y in lines[middle_lines[0]]['y'] if y >= value])
            x_point = lines[line]['x'][lines[line]['y'].index(y_point)] 
            y_above_point = max([y for y in lines[line]['y'] if y <= value])
            x_above_point = lines[line]['x'][lines[line]['y'].index(y_above_point)] 
            new_x = (((value - y_point) * (x_above_point - x_point)) / (y_above_point - y_point)) + x_point
            x.append(int(new_x))
        x.append(int(value))
        x.append(video_status["weight_meters"])
        set_lines.append(x)
            
    return set_lines[0], set_lines[1]

#detect_lane = test.Test()
SAVE_IMAGE = True
lane_detector = LaneDetector()

with open('data_video.json') as file:
    data = json.load(file)
video_status = data['data_video'][0]
videopath = video_status['path']
assert ops.exists(videopath), '{:s} not exist'.format(videopath)

video_name = video_status['name_video']

car_registration_first = {}  
car_registration_second = {}
car_registration_write_first = {}
car_registration_write_second = {}

colors = [
    (255,0,0),
    (0,255,0),
    (0,0,255),
    (255,0,255),
    (128,0,0),
    (0,128,0),
    (0,0,128),
    (128,0,128),
    (128,128,0),
    (0,128,128)
    ]

vid = cv2.VideoCapture(videopath)
mot_tracker = Sort() 

fourcc = cv2.VideoWriter_fourcc(*'XVID')
ret,frame=vid.read()
vh, vw, rt = frame.shape
fps = vid.get(cv2.CAP_PROP_FPS)  
'''
nombre del video de salida
nombre del archivo de registro general, este contempla todos los registros de autos
'''

os.makedirs(f'data_register/{video_status["site"]}', exist_ok=True)
register = open(f'data_register/{video_status["site"]}/register_total_detection_{video_name}.csv', 'w')
register_cross = open(f'data_register/{video_status["site"]}/register_cross_{video_name}.csv', 'w') 
os.makedirs(f'data_register/{video_status["site"]}/video_output', exist_ok=True)
outvideo = cv2.VideoWriter(f'data_register/{video_status["site"]}/video_output/salida_{video_name}.avi', fourcc, fps, (vw,vh))
'''
necesito ajustar estos valores desde un input
tambien necesito un registro general de los anchos de cada carretera en un archivo, tiene que ser en metros y en google maps
date_detect = [pista izquierda, pista_derecha, pizel de linea horizontal, ancho de pista en metros]
como son dos pistas deben ser 2 "date_detect"
'''
date_detect_f = [479 , 728, 535, 3.48]

date_detect_s = [515, 638, 329, 3.48] 


# nombre de las columnas de excel 
register.write('id_car;x0;y0;x1;y1;medio\n')
# registro de cruce en zona
register_cross.write('lane_left(pixeles): ;{0}\nlane_rigth(pixeles): ;{1}\nlane_base(pixeles): ;{2}\ntrack_width(meters): ;{3}\n'.format(str(date_detect_f[0]), str(date_detect_f[1]), str(date_detect_f[2]), date_detect_f[3]))
#register_cross.write('lane_left(pixeles) second: ;{0}\nlane_rigth(pixeles) second: ;{1}\nlane_base(pixeles) second: ;{2}\ntrack_width(meters): ;{3}\n'.format(str(date_detect_s[0]), str(date_detect_s[1]), str(date_detect_s[2]), date_detect_s[3]))
register_cross.write('id_car;time_cross;distance_pixel;dintance_meters;vehicle_width\n')
frames = 0
starttime = time.time()
os.makedirs(f'data_register/{video_status["site"]}/image_cross', exist_ok=True)



while(True):

    ret, frame = vid.read()

    if (frames == 0) or (frames % 30 == 0):

        frame_lanes, transpose_frame_lanes = lane_detector.image_process(frame, skip_plots=True)
        sorted_lanes = [lane_key for lane_key, _ in sorted(transpose_frame_lanes.items(), key=lambda l: min(l[1]['x']))]
        y_vertices_coordinates = {
            lane_key: {'min': min(lane['y']), 'max': max(lane['y'])} for lane_key, lane in transpose_frame_lanes.items()
        }
        vertices_coordinates = {
            lane: {
                agg: (find_x_coordinate(transpose_frame_lanes[lane], y_vertices_coordinates[lane][agg]), y_vertices_coordinates[lane][agg])
                for agg in ['min', 'max']
            } for lane in sorted_lanes
        }

        date_detect_f, date_detect_s = interpolation_2d(vertices_coordinates, transpose_frame_lanes, vh)
    
    #import pdb; pdb.set_trace()
    if not ret:
        break

    if frames == 1800: # ajuste de 15 minutos.
        break 

    frames += 1
    img_adjusted = np.zeros((vh, vw, 3), np.uint8) #esto hace que la imagen este negra
    '''
    vertices: me permite apartar todo lo demas que no sea pista y lo pongo en negro.
    
    '''

    # points that generate the polygon in the mask 
    vertices = np.array([[(0, vh),
            (vertices_coordinates[list(vertices_coordinates.keys())[0]]['max']),
            (visual_adjustment(vertices_coordinates)[0], vertices_coordinates[list(vertices_coordinates.keys())[0]]['min'][1]),
            (visual_adjustment(vertices_coordinates)[1], vertices_coordinates[list(vertices_coordinates.keys())[-1]]['min'][1]), 
            (vertices_coordinates[review_order(vertices_coordinates)[-1]]['max'][0], vh)]], 
            dtype=np.int32
        )
    mask = np.zeros_like(frame)
    ignore_mask_color = (255,)*rt 
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    frame = cv2.bitwise_and(frame, mask) # Esta variable debe llevar otro nombre, para poder escribir sobtr la original 
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    if frames == 1:
        cv2.imwrite(f'mask_{video_name}.png', frame)
        
    pilimg = Image.fromarray(frame)
    detections = detect_image(pilimg)

    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    img = np.array(pilimg)
    pad_x = max(img.shape[0] - img.shape[1], 0) * (img_size / max(img.shape))
    pad_y = max(img.shape[1] - img.shape[0], 0) * (img_size / max(img.shape))
    unpad_h = img_size - pad_y
    unpad_w = img_size - pad_x

    if detections is not None:
        tracked_objects = mot_tracker.update(detections.cpu())
        unique_labels = detections[:, -1].cpu().unique()
        n_cls_preds = len(unique_labels)
        for x1, y1, x2, y2, obj_id, cls_pred in tracked_objects:
            box_h = int(((y2 - y1) / unpad_h) * img.shape[0])
            box_w = int(((x2 - x1) / unpad_w) * img.shape[1])
            y1 = int(((y1 - pad_y // 2) / unpad_h) * img.shape[0])
            x1 = int(((x1 - pad_x // 2) / unpad_w) * img.shape[1])
            color = colors[int(obj_id) % len(colors)]
            cls = classes[int(cls_pred)]
            # deteccion de cruce del auto
            detect_line_crossing(points = [x1, y1, x1+box_w, y1+box_h], frames = frames, id_car = obj_id, 
                                document = register_cross, date_detect = date_detect_f, car_registration = car_registration_first, 
                                car_registration_write = car_registration_write_first, state = 'First')
            
            detect_line_crossing(points = [x1, y1, x1+box_w, y1+box_h], frames = frames, id_car = obj_id, 
                                document = register_cross, date_detect = date_detect_s, car_registration = car_registration_second, 
                                car_registration_write = car_registration_write_second, state = 'Second')
            
            cv2.rectangle(frame, (x1, y1), (x1+box_w, y1+box_h), color, 4)
            cv2.rectangle(frame, (x1, y1-35), (x1+len(cls)*19+80, y1), color, -1)
            cv2.putText(frame, cls + "-" + str(int(obj_id)), (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 3)
            ps = int(x1 + (box_w/2))
            register.write('{0};{1};{2};{3};{4};{5}\n'.format(obj_id, x1, y1, x1+box_w, y1+box_h, ps))

    if state_cross['First'] and SAVE_IMAGE == True:
        save_image(frame, date_detect_f, state = 'First') 
    if state_cross['Second'] and SAVE_IMAGE == True:    
        save_image(frame, date_detect_s, state = 'Second') 

    outvideo.write(frame)
    ch = 0xFF & cv2.waitKey(1)
    if ch == 27:
        break

totaltime = time.time()-starttime
print(frames, "<-frames", totaltime, "<-s")
write_two_detection(car_registration_first, car_registration_second, video_name)
cv2.destroyAllWindows()
outvideo.release()
register.close()
register_cross.close()
